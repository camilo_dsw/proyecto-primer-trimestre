<?php

class Post
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getPosts()
    {
        $this->db->query("SELECT *, 
        garaje.id as postId,
        garaje.created_at as postCreatedAt,
        users.id as userId, 
        users.created_at as userCreatedAt
        FROM  garaje 
        INNER JOIN users 
        ON garaje.user_id = users.id
        ORDER BY garaje.created_at DESC ");

        $results = $this->db-> resultSet('Post');

        return $results;
    }

    public function addPost($data)
    {

        $this->db->query("INSERT into garaje(user_id, matricula, plaza,image)values(:id, :matricula, :plaza, :image)");
        $this->db->bind(':id', $data['user_id']);
        $this->db->bind(':matricula', $data['matricula']);
        $this->db->bind(':plaza', $data['plaza']);
        $this->db->bind(':image', $data['image']);
        $result = $this->db->execute();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getPostById($id){
        $this->db->query("SELECT * from garaje where id = :id");
        $this->db->bind(':id', $id);
        $result = $this->db->obtnerUsuario('Post');
        return $result;
    }

    public function updatePost($data){
        $this->db->query("UPDATE garaje set matricula = :matricula, plaza = :plaza, image = :image where id = :id");
        $this->db->bind(':matricula', $data['matricula']);
        $this->db->bind(':plaza', $data['plaza']);
        $this->db->bind(':image', $data['image']);
        $this->db->bind(':id', $data['id']);
        $result = $this->db->execute();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deletePost($id){
        $this->db->query("DELETE from garaje where id = :id");
        $this->db->bind(':id', $id);
        $result = $this->db->execute();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    
}
