<?php

use Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{

    private $postModel;
    private $userModel;

    public function __construct()
    {
        //Protege la vista posts mediante la función isLoggedIn
        // Para ello comprueba en el constructor de Posts, con la función isLoggedIn, si un usuario no está  logueado
        if (!isLoggedIn()) {
            urlRedirect('/users/login');
        }

        $this->postModel = $this->model('Post');
        $this->userModel = $this->model('User');
    }

    // método index que cargue la vista index de post pasándole el array $data.

    public function index()
    {

        $posts = $this->postModel->getPosts();

        $data = [
            'titulo' => 'Index de posts',
            'posts' => $posts
        ];

        return $this->view('posts/index', $data);
    }


    public function add()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $flag_err = true;

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'matricula' => trim($_POST['matricula']),
                'plaza' => trim($_POST['plaza']),
                'image' => trim($_FILES['image']['name']),
                'user_id' => $_SESSION['id'],
                'matricula_err' => '',
                'plaza_err' => '',
                'image_err' => ''
            ];



            if (empty($data['matricula'])) {

                $data['matricula_err'] = 'Tienes poner una matrícula';
                $flag_err = false;
            } else {
                $data['matricula_err'] = '';
            }

            if (empty($data['plaza'])) {
                $data['plaza_err'] = 'Tienes que introducir el número de plaza';
                $flag_err = false;
            } else {

                $data['plaza_err'] = '';
            }
            if (empty($data['image'])) {
                $data['image_err'] = 'Tienes que introducir el fichero imagen';
                $flag_err = false;
            } else {

                $data['image_err'] = '';
            }

            if (!empty($data['image'])) {
               
                if ($_FILES['image']['error'] !== UPLOAD_ERR_OK) {
                    switch ($_FILES['image']['error']) {
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                    $data['image_err'] = 'El fichero es demasiado grande. Tamaño máximo: '.ini_get('upload_max_filesize');
                    break;
                    case UPLOAD_ERR_PARTIAL:
                    $data['image_err'] = "No se ha podido subir el fichero completo";
                    break;
                    default:
                    $data['image_err'] = "Error al subir el fichero";
                    break;
                    }
                }
                else {
                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    if (in_array($_FILES['image']['type'], $arrTypes) === false) {
                    $data['image_err'] = "Tipo de fichero no soportado";
                    } else if (is_uploaded_file($_FILES['image']['tmp_name']) === false) {
                    $data['image_err'] = "El archivo no se ha subido mediante un formulario";
                    } else if (move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
                    $data['image_err'] = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
                    }
                }
                    if(!empty($data['image_err'])){
                        $flag_err=false;
                    }

                }







            // Si los campos de error están vacíos, insertamos. 
            if ($flag_err) {
                //var_dump($data);

                $this->postModel->addPost($data);
                $flash = new Flash();
                $flash->message('Se ha reservado una nueva plaza de garaje.');
                urlRedirect('/posts/index');
            } else {
                // Si hay algún error
                $this->view('posts/add', $data);
            }
        } else {

            $data = [
                'matricula' => '',
                'plaza' => '',
                'image' => '',
                'user_id' => '',
                'matricula_err' => '',
                'plaza_err' => '',
                'image_err' => ''
            ];

            return $this->view('posts/add', $data);
        }
    }

    public function show($id)
    {
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($post->user_id);

        $data = [
            'user' => $user,
            'post' => $post
        ];

        return $this->view('posts/show', $data);
    }


    public function edit($id)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'id' => $id,
                'matricula' => trim($_POST['matricula']),
                'plaza' => trim($_POST['plaza']),
                'image' => trim($_FILES['image']['name']),
                'user_id' => $_SESSION['id'],
                'matricula_err' => '',
                'plaza_err' => '',
                'image_err' => ''
            ];

            // comprobamos si los campos están vacios
            $flag_err = true;
            if (empty($data['matricula'])) {
                $data['matricula_err'] = 'Tienes que introducir una matrícula';
                $flag_err = false;
            } else {
                $data['matricula_err'] = '';
            }

            if (empty($data['plaza'])) {
                $data['plaza_err'] = 'Tienes que introducir un número de plaza';
                $flag_err = false;
            } else {
                $data['plaza_err'] = '';
            }

            if (empty($data['image'])) {
                $data['image_err'] = 'Tienes que introducir una imagen';
                $flag_err = false;
            } else {
                $data['image_err'] = '';
            }

            // Si los campos de error están vacíos, insertamos. 
            if ($flag_err) {
                $this->postModel->updatePost($data);
                $flash = new Flash();
                $flash->message('La reserva se ha actualizado');
                urlRedirect('/posts/index');
            } else {
                // mostramos errores
                $this->view('posts/edit', $data);
            }
        } else {

            $post = $this->postModel->getPostById($id);

            if ($post->user_id !== $_SESSION['id']) {
                $flash = new Flash();
                $flash->error('No tienes permisos para editar estos datos');
                urlRedirect('/posts/index');
            }

            $data = [
                'id' => $id,
                'matricula' => $post->matricula,
                'plaza' => $post->plaza,
                'image' => $post->image,
                'user_id' => $post->user_id,
            ];

            return $this->view('posts/edit', $data);
        }
    }

    public function delete($id){
        

        if( $_SERVER['REQUEST_METHOD'] == 'POST' ){

            $post = $this->postModel->getPostById($id);

            if ($post->user_id !== $_SESSION['id']) {
                $flash = new Flash();
                $flash->error('Necesitas permisos para eliminar esta reserva');
                urlRedirect('/posts/index');
            }else{

                
                    $this->postModel->deletePost($post->id);
                    $flash = new Flash();
                    $flash->message('Se ha eliminado la reserva.');
                    urlRedirect('/posts/index');
                  }
        }else{
            
            $flash = new Flash();
            $flash->error('Necesitas permisos para eliminar esta reserva.');
            urlRedirect('/posts/index');
        }
    }











}
