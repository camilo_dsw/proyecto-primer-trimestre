<?php require APPROOT . '/views/partials/header.php'; ?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2> Crear una cuenta</h2>
            <p>Por favor llena los campos para poder registrarse</p>
            <form action="<?= URLROOT . '/users/register'; ?>" class="needs-validation" novalidate method="POST">
                <!-- usamos esta clase de bootstrap-->
                <div class="form-group">
                    <label for="name">Nombre: <sup>*</sup></label>
                    <input type="text" name="name" class="form-control <?php if (isset($data['name_err']) && $data['name_err'] == '') {
                                                                            echo '';
                                                                        } elseif (isset($data['name_err']) && $data['name_err'] !== '') {
                                                                            echo 'is-invalid';
                                                                        } else {
                                                                            echo 'is-valid';
                                                                        }   ?>" value="<?php if (isset($_POST['name'])) {
                                                                                            echo $data['name'];
                                                                                        } else {
                                                                                            echo "";
                                                                                        }
                                                                                        ?>">
                    <?php if (isset($data['name_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['name_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            Nombre válido
                        </div>
                    <?php endif; ?>


                </div>
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control <?php if (isset($data['email_err']) && $data['email_err'] == '') {
                                                                                echo '';
                                                                            } elseif (isset($data['email_err']) && $data['email_err'] !== '') {
                                                                                echo 'is-invalid';
                                                                            } else {
                                                                                echo 'is-valid';
                                                                            }   ?>" value="<?php if (isset($_POST['email'])) {
                                                                                                echo $data['email'];
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>">

                    <?php if (isset($data['email_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['email_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            Email correcto
                        </div>
                    <?php endif; ?>


                </div>
                <div class="form-group">
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?php if (isset($data['password_err']) && $data['password_err'] == '') {
                                                                                    echo '';
                                                                                } elseif (isset($data['password_err']) && $data['password_err'] !== '') {
                                                                                    echo 'is-invalid';
                                                                                } else {
                                                                                    echo 'is-valid';
                                                                                } ?>" value="<?php if (isset($_POST['password'])) {
                                                                                                    echo $data['password'];
                                                                                                } else {
                                                                                                    echo "";
                                                                                                } ?>">

                    <?php if (isset($data['password_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['password_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            Contraseña Valida
                        </div>
                    <?php endif; ?>

                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirmar contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control<?php if (isset($data['confirm_password_err']) && $data['confirm_password_err'] == '') {
                                                                                            echo '';
                                                                                        } elseif (isset($data['confirm_password_err']) && $data['confirm_password_err'] !== '') {
                                                                                            echo 'is-invalid';
                                                                                        } else {
                                                                                            echo 'is-valid';
                                                                                        }  ?> " value="<?php if (isset($_POST['confirm_password'])) {
                                                                                                            echo $data['confirm_password'];
                                                                                                        } else {
                                                                                                            echo "";
                                                                                                        } ?>">
                    <?php if (isset($data['confirm_password_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['confirm_password_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            Las contraseñas son iguales
                        </div>
                    <?php endif; ?>
                </div>

        </div>
        <div class="row">
            <div class="col">
                <a href="<?= URLROOT . '/users/login'; ?>">¿Ya tienes cuenta? Inicia sesión</a>
            </div>
            <div class="col">
                <input type="submit" value="Registrar" class="btn btn-primary btn-block">
            </div>
        </div>
        </form>
    </div>
</div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>