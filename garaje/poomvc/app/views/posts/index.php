<?php require_once APPROOT . '/views/partials/header.php'; ?>
<div class="container">

    <div class="row mb-3">
        <div class="flashes">
            <?= (string) flash() ?>
        </div>

        <div class="col-md-6">
            <br>
            <h1>Plazas de Garajes</h1>
        </div>
        <div class="col-md-6">
            <br>
            <a class="btn btn-primary pull-right" href="<?= URLROOT . '/posts/add' ?>" role="button">
                <i class="fas fa-pencil-alt"></i> Crear la nueva plaza de garaje
            </a>
        </div>
        <?php foreach ($data['posts'] as $post) : ?>
            <div class="card" style="width: 60rem;">
                <div class="card-header">
                 <h5>   Cliente: <?= $post->name ?> | Fecha y  hora de registro: <?= $post->postCreatedAt ?> </h5>

                </div>
                <div class="card-body">
                    <h3 class="card-title">Matrícula: <?= $post->matricula ?></h3>
                    <p class="card-text"><h3>Número de plaza: <?= $post->plaza ?></h3></p>
                    <p><h3 align = "right">Foto del coche: </h3></p>
                    <img style="width: 200px" align = "right" border="1" alt="No a aportado imagen de coche" width="200" height="150" src ="../img/<?= $post->image ?>" alt="..." >
                    <br>
                    <a href="<?= URLROOT . "/posts/show/$post->postId" ?>" class="btn btn-primary">Editar o borrar</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php require_once APPROOT . '/views/partials/footer.php'; ?>