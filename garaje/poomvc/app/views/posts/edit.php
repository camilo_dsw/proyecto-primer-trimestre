<?php require_once APPROOT . '/views/partials/header.php'; ?>
<a class="btn btn-warning pull-right" href="<?=URLROOT . '/posts/index'  ?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5" style="width: 45rem;">
    <h2>Editar plaza de garaje</h2>
    <p>Introduzca algún cambio si lo necesita, gracias.</p>
    <form method="POST" action="<?= URLROOT . '/posts/edit/' .$data['id']?>" enctype='multipart/form-data'>
        <div class="form-group" style="width: 35rem;">
            <label for="matricula">Plaza de garaje: <sup>*</sup></label>
            <input type="text" name="matricula" class="form-control <?= empty($data['matricula_err']) ? '' : 'is-invalid' ?>" 
            placeholder="Número de matrícula" value="<?= isset($data['matricula']) ? $data['matricula'] : ''?>">
            <span class="invalid-feedback">
                <?= isset($data['matricula_err']) ? $data['matricula_err'] : '' ?>
            </span>
        </div>
        <div class="form-group"  style="width: 35rem;">
            <label for="plaza">Número de plaza de garaje: <sup>*</sup></label>
            <input type="text" name="plaza" class="form-control <?= empty($data['plaza_err']) ? '' : 'is-invalid' ?>"  
            placeholder="Número de plaza" value=" <?= isset($data['plaza']) ? $data['plaza'] : ''?>">
               
            
            
            <span class="invalid-feedback">
                <?= isset($data['plaza_err']) ? $data['plaza_err'] : '' ?>
            </span>

        </div>
        <div class="form-group" style="width: 30rem;">
            <label for="image">Introduzca una foto del coche: <sup>*</sup></label>
            <input type="file" name="image" size="20" class="form-control <?= empty($data['image_err']) ? '' : 'is-invalid' ?> " rows="1" 
            placeholder="Foto del coche" value=" <?= isset($data['image']) ? $data['image'] : ''?>">
            

            <span class="invalid-feedback">
                <?= isset($data['image_err']) ? $data['image_err'] : '' ?>
            </span>
        </div>

        <div class="row">
            <div class="col">
                <input type="submit" value="Guardar los nuevos cambios" class="btn btn-primary btn-block">
            </div>
        </div>
       
        
    </form>

    





</div>
<?php require_once APPROOT . '/views/partials/footer.php'; ?>